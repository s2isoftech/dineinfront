import { Pipe, PipeTransform } from '@angular/core';
import { isArray } from 'util';

@Pipe({
  name: 'biller'
})
export class BillerPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let res: any;
    if (isArray(value)) {
      for (let i = 0; i < value.length; i++) {
        res = res + (value[i].price * value[i].quantity);
      }
    }
    return res;
  }

}
