import { Component } from '@angular/core';
import { DataService } from './services/data.service';
import { isArray } from 'util';
import { Router } from '@angular/router';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  items: any;
  profile: any;
  orders: any;
  total: number;
  categories: any = [];
  showReview = false;
  constructor(private dataService: DataService,
    private router: Router) {
    this.dataService.menuItems().then(res => {
      const data: any = res;
      this.items = data.menu.items;
      this.categories = data.menu.categories;
    });

    this.dataService.restaurantProfile(43).then(res => {
      const data: any = res;
      this.profile = data.profile;
    });

    /* this.dataService.prepareOrder().then(res => {
      const data: any = res;
      this.items = data.menu;
    }); */
  }

  openItemDetails(item: any) {
    console.log(JSON.stringify(item));
  }

  placeOrder(items) {
    // this.router.navigate(['/payment'], items);
    const orderItems = [];
    for (let i = 0; i < items.length; i++) {
      if (isArray(items) && items[i] != null && items[i].quantity > 0) {
        const item = {
          name : items[i].name,
          price: items[i].price,
          quantity: items[i].quantity,
          subTotal: items[i].price * items[i].quantity
        };
        orderItems.push(item);
        items[i].quantity = 0;
      }
    }
    this.total = this.calculateBill();
    this.orders = {'items': orderItems, 'total' : this.calculateTotalBill(orderItems)};
  }
  addItem(item) {
    for (let i = 0; i < this.items.length; i++ ) {
      if (this.items[i].id === item.id) {
        this.items[i].quantity = this.items[i].quantity + 1;
      }
    }
    this.calculateBill();
  }

  removeItem(item) {
    for (let i = 0; i < this.items.length; i++ ) {
      if (this.items[i].id === item.id) {
        if (this.items[i].quantity > 0) {
          this.items[i].quantity = this.items[i].quantity - 1;
        }
      }
    }
    this.calculateBill();
  }

  calculateBill () {
    let res = 0;
      for (let i = 0; i < this.items.length; i++) {
        res = res + (this.items[i].price * this.items[i].quantity);
      }
    if (res > 0) {
      this.showReview = true;
    } else {
      this.showReview = false;
    }
    this.total = res;
    return res;
  }
  calculateTotalBill (orderItems): number {
    let orderTotal = 0;
      for (let i = 0; i < orderItems.length; i++) {
        orderTotal = orderTotal + orderItems[i].subTotal;
      }
    return orderTotal;
  }
}
