import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class DataService {
  myModel: any = 1000;
  constructor(private httpClient: HttpClient) { }
  // Get menu items
  menuItems() {
    const res = this.httpClient
    .get('http://localhost:8080/menu')
    .toPromise();
    return res;
  }
  // Keep Order in memory before submit
  prepareOrder() {

  }
  // Get Restaurant Profile
  restaurantProfile(id) {
    return this.httpClient
    .get('http://localhost:8080/restaurant/find/' + id)
    .toPromise();
  }
}
